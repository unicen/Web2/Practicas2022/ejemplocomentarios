<?php
    define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

    require_once("./Controller/ComentariosController.php");
    require_once("./Controller/AuthController.php");

    $action = 'showAll';

    if(isset($_GET['action']) && !empty($_GET['action'])){
        $action = $_GET['action'];
    }


    switch ($action) {
        case 'login':
            $controller = new AuthController();
            $controller->showFormLogin();
            break;

        case 'validate':
            $controller = new AuthController();
            $controller->validateUser();
            break;

        case 'logout':
            $controller = new AuthController();
            $controller->logout();
            break;


        case 'showAll':
            $controller = new ComentariosController();
            $controller->showAll();
            break;

        case 'post':
            $controller = new ComentariosController();
            $controller->post();
            break;       
        default:
            echo "404";
            break;
    }