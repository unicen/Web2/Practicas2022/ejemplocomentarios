{include 'header.tpl'}

{$nota}
<ul class="list-group">
    {foreach from=$comentarios item=$comentario}
        <li class='
                list-group-item d-flex justify-content-between align-items-center
            '>
            {$comentario->id}: {$comentario->texto}
        </li>
    {/foreach}
</ul>
{include file='footer.tpl'}
