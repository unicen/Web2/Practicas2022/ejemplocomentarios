<?php

    class ComentariosModel
    {
        private $db;

        public function __construct() {
            $this->db = new PDO('mysql:host=localhost;'.'dbname=comentarios;charset=utf8', 'root', '');
        }
    
        public function getAll()
        {
            $query = $this->db->prepare("SELECT * FROM comentario");
            $query->execute();
    
            // 3. obtengo los resultados
            $tasks = $query->fetchAll(PDO::FETCH_OBJ); // devuelve un arreglo de objetos
            
            return $tasks;
    
        }

    }
    