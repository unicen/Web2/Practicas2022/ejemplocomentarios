<?php
    require_once("./Model/ComentariosModel.php");
    require_once("./View/ComentariosView.php");

    class ComentariosController {
        private $comentariosModel;
        private $view;

        function __construct(){
            $this->helper = new AuthHelper();
            $this->comentariosModel = new ComentariosModel();
            $this->view = new ComentariosView($this->helper->getUser());
        }

        public function showAll()
        {
            $comentarios = $this->comentariosModel->getAll();
            $this->view->showAll($comentarios, "Estos son los comentarios:");
        }

        public function post()
        {
            $this->helper->checkLoggedIn();
            // leer $_POST
            // chequear datos
            // this->model->insert($datos)
            header("Location: " . BASE_URL);
        }
    }