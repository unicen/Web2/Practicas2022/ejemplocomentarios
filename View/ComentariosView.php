<?php
    require_once './libs/smarty-4.2.1/libs/Smarty.class.php';

    
    class ComentariosView {
        private $smarty;

        public function __construct($user) {
            $this->smarty = new Smarty(); // inicializo Smarty
            $this->smarty->assign('user', $user);
        }
    
        public function showAll($comentarios, $titulo)
        {
            // asigno variables al tpl smarty
            $this->smarty->assign('nota', $titulo);
            $this->smarty->assign('comentarios', $comentarios);


            // mostrar el tpl
            $this->smarty->display('comentariosList.tpl');
        }
    }